//Capta la tecla de espacio
document.addEventListener('keydown', function(e) {
    if(e.keyCode == 32){
        if(nivel.muerto == false){
            saltar();
        }else{
            nivel.velocidad = 9;
            nube.velocidad = 2;
            nube.x = ancho + 100;
            cactus.x = ancho + 100;
            nivel.marcador = 0
            nivel.muerto = false;
        }
    }
});

//Creacion de imagenes con carga desde carptea img
var imgRex, imgNube, imgCactus, imgCactusDuo, imgSuelo;
function cargaImg(){
    imgRex = new Image();
    imgNube = new Image();
    imgCactus = new Image();
    imgCactusDuo = new Image();
    imgSuelo = new Image();

    imgRex.src = 'img/rex.png';
    imgNube.src = 'img/nube.png';
    imgCactus.src = 'img/cactus-1.png';
    imgCactusDuo.src = 'img/cactus-2.png';
    imgSuelo.src = 'img/suelo.png';
}


var ancho = 700;
var alto = 300;
var canvas, ctx;

//Funcion de inicio del canvas
function inicializar(){
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    cargaImg();
}

//Eliminar imagenes entre fotogramas
function borrarCanvas(){
    canvas.width = ancho;
    canvas.height = alto;
}

//Bibuja a rex en canvas
var suelo = 250;
var tRex = {
    y: 240,
    vy: 0,
    gravedad: 2,
    salto: 28,
    vyMax: 9,
    saltando: false
};
var nivel = {
    velocidad: 9,
    marcador: 0,
    muerto: false
};
var cactus = {
    x: ancho + 100,
    y: suelo - 25
};
var nube = {
    x: 400,
    y: 100,
    velocidad: 2
};
var sueloGrafico = {
    x: 0,
    y: suelo
};

function dibujarRex() {
    ctx.drawImage(imgRex,0,0,32,32,30,tRex.y,50,50);
}

//--------------------------------------------------
function dibujarCactus(){
    ctx.drawImage(imgCactus,0,0,32,32,cactus.x,cactus.y,50,50);
}
function logicaCactus(){
    if (cactus.x < -100){
        cactus.x = ancho + 100;
        nivel.marcador++;
    }else{
        cactus.x -= nivel.velocidad;
    }
}

//--------------------------------------------------
function dibujarNube(){
    ctx.drawImage(imgNube,0,0,32,32,nube.x,nube.y,92,31);
}
function logicaNube(){
    if (nube.x < -100){
        nube.x = ancho + 100;
    }else{
        nube.x -= nube.velocidad;
    }
}

//--------------------------------------------------
function dibujarSuelo(){
    ctx.drawImage(imgSuelo,sueloGrafico.x,0,700,32,0,sueloGrafico.y,700,32);
}
function logicaSuelo(){
    if (sueloGrafico.x > 700){
        sueloGrafico.x = 0;
    }else{
        sueloGrafico.x += nivel.velocidad;
    }
}

//Salto
function saltar() {
    tRex.saltando = true;
    tRex.vy = tRex.salto;
}

//Gravedad
function gravedad(){
    if (tRex.saltando == true) {
        if (tRex.y - tRex.vy - tRex.gravedad > suelo) {
            tRex.saltando = false;
            tRex.vy = 0;
            tRex.y = suelo;
        }else{
            tRex.vy -= tRex.gravedad;
            tRex.y -= tRex.vy;
        }
    }
}

//Colisiones
function collision(){
    if (cactus.x >=100 && cactus.x <= 150) {
        if (tRex.y > suelo - 25){
            nivel.muerto = true;
            nivel.velocidad = 0;
            nube.velocidad = 0
        }
    }
}

//Puntuacion
function puntuacion(){
    ctx.font = "30px impact";
    ctx.fillStyle = "#555";
    ctx.fillText(`${nivel.marcador}`,600,50);

    if (nivel.muerto == true){
        ctx.font = "60px impact";
        ctx.fillText(`GAMER OVER`,240,150)
    }
}

//--------------------------------------------------

//Buclke principal
var FPS = 50;
setInterval(function() {
    principal();
}, 1000/FPS);

//Llama las funciones para ser ejecutadas
function principal(){
    borrarCanvas();
    gravedad();
    collision();
    logicaCactus();
    logicaNube();
    logicaSuelo();
    dibujarCactus();
    dibujarNube();
    dibujarSuelo();
    dibujarRex();
    puntuacion();
}